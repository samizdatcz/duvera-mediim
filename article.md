---
title: "Studie: Důvěra v média je rekordně nízká. Nevěří jim mladí dospělí nebo voliči levice"
perex: "Novinám a časopisům věří jen třetina Čechů, televizi o dvě procenta víc, rádiím 45 procent. Podle studie Masarykovy univerzity vyčítají čtenáři novinářům hlavně to, že jsou příliš bulvární – a také úplatní."
description: "Novinám a časopisům věří jen třetina Čechů, televizi o dvě procenta víc, rádiím 45 procent. Podle studie Masarykovy univerzity vyčítají čtenáři novinářům hlavně to, že jsou příliš bulvární – a také úplatní."
authors: ["Jan Boček"]
published: "9. března 2017"
coverimg: https://interaktivni.rozhlas.cz/data/duvera-mediim/www/media/coverimg2.jpg
coverimg_note: "Foto <a href='https://www.flickr.com/photos/tercik10/7651459900/'>Tereza Čechová: Old man and newspaper</a>"
socialimg: https://interaktivni.rozhlas.cz/data/duvera-mediim/www/media/socialimg.png
url: "duvera-mediim"
libraries: [jquery, highcharts]
recommended:
  - link: https://interaktivni.rozhlas.cz/rozdelena-spolecnost/
    title: Rozdělená společnost?
    perex:  Čechy proti sobě staví věk a vzdělání
    image: https://interaktivni.rozhlas.cz/rozdelena-spolecnost/media/7d29d198e53cdad18df2d865db3c0f5d/450x_.jpg
  - link: https://interaktivni.rozhlas.cz/duvera-politikum/
    title: Analýza ČRo: Sobotka u voličů ČSSD neztrácí důvěru
    perex: Říjnová data z průzkumu veřejného mínění ukazují, že kauzy posledních měsíců Sobotkovi neubližují.
    image: https://interaktivni.rozhlas.cz/media/a5c1edc60116e7c2760e255090258265/600x_.jpg
  - link: https://interaktivni.rozhlas.cz/duvera-prezidentum/
    title: Zeman je prezidentem voličů ČSSD, KSČM a ANO
    perex: Detailní analýza průzkumů veřejného mínění ukazuje, kdo jsou prezidentovi věrní.
    image: https://interaktivni.rozhlas.cz/duvera-prezidentum/media/536c67c76ead9dd8ba2ed9ee776eabdf/450x_.jpg
---

V březnu 2004 řekly dvě třetiny dotázaných agentuře CVVM: ano, věříme tomu, co vidíme a slyšíme v televizních zprávách. Loni v říjnu už na stejnou otázku odpověděla kladně jen třetina lidí. Ostatní sdělovací prostředky ztrácejí důvěru veřejnosti jen o trochu pomaleji. Čím to je? A mohou za to novináři?

<aside class="big">
  <div id="duvera" style="height:600px;">
</aside>

Data ukazují, že u tištěných médií začal propad důvěry před deseti lety, tedy těsně před tím, než česká média naplno pocítila ztrátu inzertních příjmů vyvolanou finanční krizí. Na přelomu let 2015 a 2016, kdy už byla ekonomika stabilizovaná, se propad důvěry ještě zrychlil: u většiny typů médií klesla během jediného roku přibližně o deset procentních bodů.

<div data-bso="1"></div>

Hledání příčin se věnuje [studie Masarykovy univerzity](https://is.muni.cz/publication/1363988), kterou publikovali Václav Moravec, Marína Urbaníková a Jaromír Volek. „Některé příčiny stávající krize žurnalistiky jsou zřejmé, některé jen tušíme. Co ale spojuje jednotlivé projevy úpadku novinářství v době jeho nejvyšší technologické exploze, souvisí s dvojí ztrátou důvěry. Klesá nejen důvěra veřejnosti v práci novinářů, respektive zpravodajských médií, ale současně narůstá skepse samotných novinářů k současnému směřování zpravodajských médií,“ uvádí studie.

Výzkumníci sledovali změnu postojů veřejnosti i samotných novinářů v letech 2004 a 2016. Oba průzkumy využily kvótního výběru na základě pohlaví, věku, vzdělání a socioekonomické pozice. V prvním odpovídalo 1084, ve druhém 1236 respondentů.

Výsledky ukazují na prudký pokles důvěry zejména u nejmladší sledované skupiny, tedy mezi 18 až 29letými. Před třinácti lety médiím nevěřilo 18 procent těchto mladých dospělých, loni to už byla nadpoloviční většina.

Následující grafy ukazují na rozdíl od první vizualizace *nedůvěru* k médiím.

<aside class="big">
  <div id="neduveravek" style="height:600px;">
</aside>

Podle výzkumníků je nedůvěra v média vedle nízkého věku stále častěji spojena také se dvěma dalšími faktory: s nízkým vzděláním a se „slabou konzumací“ – tedy s tím, jak často respondenti nějaké médium skutečně sledují.

„Zdá se, že v průběhu poslední dekády nedůvěřiví mediální konzumenti jednak výrazně omládli, a současně již nejde o formálně nejvzdělanější respondenty, u kterých bylo možné předpokládat vyšší míru kritické mediální gramotnosti,“ uvádějí autoři studie. „Naopak, v současnosti médiím nedůvěřují konzumenti s nejnižším formálním vzděláním, u těch narostla nedůvěra nejvýrazněji – z 19 procent na 49 procent. Žurnalisté pracující pro média hlavního proudu každopádně ztrácejí svá v dlouhodobém horizontu nejperspektivnější publika.“

## Novináři tíhnou doprava, většina populace doleva

Vůbec nejčastější je nedůvěra k médiím mezi nezaměstnanými a nízkopříjmovými skupinami populace. Jak uvádějí výzkumníci, data v tomto případě potvrzují jejich očekávání.

„Novináři pracující pro ‚velká média‘ se stávají nedůvěryhodnými pro ty respondenty, kteří se v různém slova smyslu ocitají na okraji společnosti. Tato zjištění procházejí celou naší studií a naznačují, že novináři a média hlavního proudu jsou vnímáni jako ti, kdo stojí na straně těch, kteří jsou úspěšnější. Podstatný je přitom výrazný posun nedůvěry mezi nejmladší a nejméně vzdělané.“

<aside class="big">
  <div id="neduvera-vzdelani" style="float: left; height:400px; width:18%"></div>
  <div id="neduvera-konzumace" style="float: left; height:400px; width:15%"></div>
  <div id="neduvera-orientace" style="float: left; height:400px; width:15%"></div>
  <div id="neduvera-vek" style="float: left; height:400px; width:15%"></div>
  <div id="neduvera-prijem" style="float: left; height:400px; width:15%"></div>
  <div id="neduvera-zamestnani" style="float: left; height:400px; width:15%"></div>
</aside>

Data ukazují také to, že nedůvěra vůči médiím je podstatně častější u voličů levice. To může souviset právě s vnímáním novinářů coby součásti establishmentu, ale také s dalším pozorovaným jevem: mezi samotnými novináři je levicová politická orientace poměrně vzácná.

„Vyšší nedůvěru vůči novinářům deklarovali levicově orientovaní respondenti, kteří jsou ovšem v populaci mírně početnější,“ vysvětlují výzkumníci. „Nejen z tohoto výzkumu je zřejmé, že mezi novináři je častější středopravicová liberální ideologie. V českém veřejném mediálním prostoru tedy stále přetrvává hodnotová asymetrie, kterou vnímají levicoví konzumenti jako výraz nedostatečné reprezentace svých zájmů a logicky reagují silnější nedůvěrou nejen k novinářské profesi.“

„Vypadá to, že nárůst distance mezi médii a jejich publiky souvisí s vysokými profesními náklady, které vynaložili čeští novináři na podporu nového politického režimu, se kterým podstatná část z nich spojila nejen své kariéry, ale i každodenní životy. Tato silná identifikace s budováním pluralitní demokracie je ale postupně konfrontována s narůstající nespokojeností většinové populace, která je spojena nejen s nerealizovanými očekáváními a nenaplněnými sliby, ale i s důsledky ekonomické krize. V domácím kontextu tak můžeme hovořit o opožděném nárůstu nedůvěry mediálních konzumentů, kteří vnímali v prvním polistopadovém desetiletí novináře na své straně. Tato nadějeplná víra s ekonomickou krizí definitivně skončila.“


Data ovšem ukazují, že v posledních letech se situace vyrovnává: od roku 2003 výrazně ubylo novinářů, kteří se hlásí k pravici.

<aside class="big">
  <div id="orientace" style="height:400px;">
</aside>

## Typický novinář? Bulvární, ale pro věšinu lidí nezávislý

Respondenti odpovídali také na otázku, jak si představují typického novináře. Mezi negativními atributy se s obrazem novináře nejčastěji spojila bulvárnost; tu žurnalistům přisuzuje téměř polovina populace.

„Téměř polovina respondentů připisuje novinářům nezávislost, což naznačuje, že nepovažují fenomén oligarchizace českých médií, respektive angažmá některých jejich vlastníků v politice, za ohrožení novinářské autonomie,“ doplňují svou interpretaci autoři studie. „Stabilní je i podíl respondentů, kteří vidí novináře jako úplatné, byť jde stále o druhý nejnegativnější atribut asociovaný s jejich obrazem.“

<aside class="big">
  <div id="hodnoty" style="height:600px;">
</aside>

Podle výzkumníků se nepoměr mezi převážně pravicovými novináři a levicovou a středovou populací obrací proti žurnalistům, kterým tak lidé méně věří. Zároveň ale dodávají, že drastický propad důvěry médií lze vysledovat i v mnoha západních zemích.

„Nedůvěra ke zpravodajským médiím a jejich žurnalistům stoupá zvláště v nejvyspělejších zemích s rozvinutou demokracií, v zemích euroamerické civilizace, kde média a jejich novináři ztrácejí pozici advokátů potřebných a strážců demokracie, a kde dochází ke stále masivnějšímu zneužívání médií k ostré polarizaci společnosti,“ uzavírají.