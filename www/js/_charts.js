var colors = ['#FF4136', '#0074D9', '#B10DC9', '#3D9970', '#FF851B', '#FFDC00','#001F3F', '#2ECC40']

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: '',
                decimalPoint:',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    var media = [
            [791596800000, 72],           // 2/1995
            [812505600000, 70],          // 10/1995
            [823132800000, 63],          // 2/1996
            [844128000000, 63],          // 10/1996
            [854755200000, 67],          // 2/1997
            [875664000000, 64],          // 10/1997
            [886291200000, 67],          // 2/1998
            [907200000000, 67],          // 10/1998
            [917827200000, 69],          // 2/1999
            [938736000000, 64],          // 10/1999
            [949363200000, 67],          // 2/2000
            [970358400000, 69],          // 10/2000
            [980985600000, 64],          // 2/2001
            [1001894400000, 71],          // 10/2001
            [1012521600000, 57],          // 2/2002
            [1046476800000, 62]           // 3/2003
        ],
        televize = [
            [1054425600000, 66],           // 6/2003
            [1064966400000, 66],           // 10/2003
            [1078099200000, 64],           // 3/2004
            [1093996800000, 67],           // 9/2004
            [1117584000000, 71],           // 6/2005
            [1128124800000, 64],           // 10/2005
            [1157068800000, 68],           // 9/2006
            [1164931200000, 66],           // 12/2006
            [1172707200000, 68],           // 3/2007
            [1188604800000, 66],           // 9/2007
            [1204329600000, 68],           // 3/2008
            [1220227200000, 64],           // 9/2008
            [1235865600000, 65],           // 3/2009
            [1251763200000, 62],           // 9/2009
            [1267401600000, 62],           // 3/2010
            [1283299200000, 58],           // 9/2010
            [1298937600000, 62],           // 3/2011
            [1314835200000, 58],           // 9/2011
            [1330560000000, 56],           // 3/2012
            [1346457600000, 52],           // 9/2012
            [1362096000000, 51],           // 3/2013
            [1377993600000, 47],           // 9/2013
            [1393632000000, 46],           // 3/2014
            [1409529600000, 51],           // 9/2014
            [1425168000000, 50],           // 3/2015
            [1443657600000, 46],           // 10/2015
            [1459468800000, 41],           // 4/2016
            [1472688000000, 35]           // 9/2016
        ],
        tisk = [
            [1054425600000, 60],           // 6/2003
            [1064966400000, 60],           // 10/2003
            [1078099200000, 59],           // 3/2004
            [1093996800000, 61],           // 9/2004
            [1117584000000, 63],           // 6/2005
            [1128124800000, 61],           // 10/2005
            [1157068800000, 58],           // 9/2006
            [1164931200000, 57],           // 12/2006
            [1172707200000, 54],           // 3/2007
            [1188604800000, 56],           // 9/2007
            [1204329600000, 57],           // 3/2008
            [1220227200000, 53],           // 9/2008
            [1235865600000, 53],           // 3/2009
            [1251763200000, 49],           // 9/2009
            [1267401600000, 50],           // 3/2010
            [1283299200000, 48],           // 9/2010
            [1298937600000, 53],           // 3/2011
            [1314835200000, 51],           // 9/2011
            [1330560000000, 50],           // 3/2012
            [1346457600000, 45],           // 9/2012
            [1362096000000, 42],           // 3/2013
            [1377993600000, 44],           // 9/2013
            [1393632000000, 42],           // 3/2014
            [1409529600000, 45],           // 9/2014
            [1425168000000, 44],           // 3/2015
            [1443657600000, 44],           // 10/2015
            [1459468800000, 36],           // 4/2016
            [1472688000000, 33]           // 9/2016
        ],
        radia = [
            [1117584000000, 70],           // 6/2005
            [1128124800000, 67],           // 10/2005
            [1157068800000, 70],           // 9/2006
            [1164931200000, 70],           // 12/2006
            [1172707200000, 71],           // 3/2007
            [1188604800000, 68],           // 9/2007
            [1204329600000, 70],           // 3/2008
            [1220227200000, 68],           // 9/2008
            [1235865600000, 66],           // 3/2009
            [1251763200000, 67],           // 9/2009
            [1267401600000, 59],           // 3/2010
            [1283299200000, 65],           // 9/2010
            [1298937600000, 67],           // 3/2011
            [1314835200000, 67],           // 9/2011
            [1330560000000, 65],           // 3/2012
            [1346457600000, 60],           // 9/2012
            [1362096000000, 58],           // 3/2013
            [1377993600000, 53],           // 9/2013
            [1393632000000, 55],           // 3/2014
            [1409529600000, 59],           // 9/2014
            [1425168000000, 58],           // 3/2015
            [1443657600000, 54],           // 10/2015
            [1459468800000, 51],           // 4/2016
            [1472688000000, 45]           // 9/2016
        ],
        internet = [
            [1314835200000, 53],           // 9/2011
            [1362096000000, 50],           // 3/2013
            [1377993600000, 44],           // 9/2013
            [1393632000000, 42],           // 3/2014
            [1409529600000, 49],           // 9/2014
            [1425168000000, 49],           // 3/2015
            [1443657600000, 46],           // 10/2015
            [1459468800000, 43],           // 4/2016
            [1472688000000, 41]           // 9/2016
        ];

    $('#duvera').highcharts({

        title: {
            text: 'Vývoj důvěry médiím'
        },

        subtitle: {
            text: 'Po ekonomické krizi v roce 2008 klesá důvěra ve všechny typy médií'
        },

        xAxis: {
            type: 'datetime',
            tickInterval: 31536000000 // 1 rok
        },

        yAxis: {
            title: {
                text: 'Důvěra typu média (%)'
            },
            min: 0
        },

        tooltip: {
            formatter: function() {
                switch(this.series.name) {
                 case 'důvěra tisku': return '<b>' + this.y + ' %</b> respondentů důvěřuje tisku.'; break;
                 case 'důvěra televizi': return '<b>' + this.y + ' %</b> respondentů důvěřuje televizi.'; break;
                 case 'důvěra rádiím': return '<b>' + this.y + ' %</b> respondentů důvěřuje rádiím.'; break;
                 case 'důvěra médiím': return '<b>' + this.y + ' %</b> respondentů důvěřuje médiím.'; break;
                 case 'důvěra internetu': return '<b>' + this.y + ' %</b> respondentů důvěřuje internetu.'; break;
                }
            },
            crosshairs: true
        },

        legend: {
        },

        exporting: {
                    enabled: false
        },

        plotOptions: {
            series: {
                    compare: 'value',
                    lineWidth: 2,
                    marker: { symbol: 'circle' }
            }
        },

        credits: {
            href : "http://nesstar.soc.cas.cz/",
            text : "Zdroj: CVVM"
        },

        series: [

        {
            name: 'důvěra médiím',
            data: media,
            zIndex: 1,
            color: colors[0],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[0]
            }
        }, {
            name: 'důvěra televizi',
            data: televize,
            zIndex: 1,
            color: colors[1],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[1]
            }
        }, {
            name: 'důvěra tisku',
            data: tisk,
            zIndex: 1,
            color: colors[2],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[2]
            }
        }, {
            name: 'důvěra rádiím',
            data: radia,
            zIndex: 1,
            color: colors[3],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[3]
            }
        }, {
            name: 'důvěra internetu',
            data: internet,
            zIndex: 1,
            color: colors[4],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[4]
            }
        }

        ]
    });
});

var colors2 = ['#FF4136', '#3D9970', '#888888']

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: '',
                decimalPoint:',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#hodnoty').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Jaké hodnoty zastává novinář?'
        },
        subtitle: {
            text: 'Nejčastějším problémem novinářů je podle české populace bulvární přístup a úplatnost'
        },
        xAxis: [{
            categories: ['bulvární', 'úplatný', 'závislý', 'přeplácený', 'nemorální', 'nevzdělaný', 'neodpovědný', 'kolaborant', 'nevyužitý', 'zbabělý', 'neužitečný']
        }, {
            categories: ['seriózní', 'neúplatný', 'nezávislý', 'podhodnocený', 'morální', 'vzdělaný', 'odpovědný', 'kritik režimu', 'přetížený', 'statečný', 'užitečný'],
            opposite: true
        }],
        yAxis: {
            max: 100,
            title: {
                text: ''
            },
            reversedStacks: false
        },
        tooltip: {
            formatter: function() {
                if (this.series.name == 'neví') {
                    return '<b>' + this.y + ' %</b> respondentů se nerozhodlo pro žádnou z odpovědí'
                } else {
                    return 'Pro <b>' + this.y + ' %</b> respondentů je typický novinář <b>' + this.x + '</b>'
                }
            },
            crosshairs: true
        },
        legend: {
            enabled: false
        },
        exporting: {
                    enabled: false
        },
        credits: {
            href : '',
            text : 'Zdroj: Studie Žurnalisté ve stínu nedůvěry: k některým příčinám klesající důvěryhodnosti českých novinářů'
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'první',
            data: [45.8, 33.9, 32.3, 31.8, 27.3, 26.0, 23.2, 21.8, 20.8, 19.9, 16.7],
            color: colors2[0]
        }, {
            name: 'neví',
            data: [25.2, 24.1, 20.5, 47.4, 19.4, 20.4, 23.3, 33.2, 43.0, 25.5, 18.3],
            color: colors2[2]
        }, {
            name: 'druhá',
            data: [33.6, 42.0, 47.2, 20.8, 53.3, 53.6, 53.5, 45.0, 36.2, 54.6, 65.7],
            color: colors2[1],
            xAxis: 1
        }]
    });
});

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#orientace').highcharts({
        chart: {
            type: 'bar',
        },
        title: {
            text: 'Politická orientace novinářů × populace'
        },
        subtitle: {
            text: 'Novináři se častěji identifikují s pravicovými stranami než česká populace'
        },
        xAxis: {
            categories: ['novináři, rok 2003', 'novináři, rok 2015', 'populace, rok 2016'],
            title: {
                text: ''
            }
        },
        yAxis: {
            title: {
                text: ''
            },
            reversedStacks: false,
            max: 100
        },
        tooltip: {
            formatter: function() {
                return '<b>' + this.x + '</b>: ' + this.y + ' % jsou <b>' + this.series.name + '</b>.'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            href : '',
            text : 'Zdroj: Studie Žurnalisté ve stínu nedůvěry: k některým příčinám klesající důvěryhodnosti českých novinářů'
        },
        plotOptions: {
            series: {
                stacking: 'stacked'
            }
        },
        series: [{
            name: 'voliči levice',
            data: [15.9, 13.9, 34.6],
            color: '#FF4136',
        }, {
            name: 'voliči středových stran',
            data: [29.8, 41.4, 35.6],
            color: '#888888',
        }, {
            name: 'voliči pravice',
            data: [54.5, 44.7, 29.8],
            color: '#0074D9',
        }]
    });
});

var colors3 = ['#cc342b', '#005cad', '#8d0aa0', '#307a59', '#cc6a15', '#ccb000'],
    anticolors3 = ['#ff665e', '#328fe0', '#c03dd3', '#63ad8c', '#ff9d48', '#ffe332']

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#neduvera-orientace').highcharts({
        chart: {
            type: 'column',
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['']
        },
        yAxis: {
            title: {
                text: ''
            },
            max: 70
        },
        tooltip: {
            formatter: function() {
                return 'Médiím nevěří <b>' + this.y + ' %</b>'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'voliči levice',
            data: [50.6],
            color: colors3[0]
        }, {
            name: 'voliči pravice',
            data: [43.1],
            color: anticolors3[0]
        }]
    });
});

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#neduvera-konzumace').highcharts({
        chart: {
            type: 'column',
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['']
        },
        yAxis: {
            title: {
                text: ''
            },
            max: 70
        },
        tooltip: {
            formatter: function() {
                return 'Médiím nevěří <b>' + this.y + ' %</b>'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'slabí konz. médií',
            data: [51.9],
            color: colors3[1]
        }, {
            name: 'silní konz. médií',
            data: [35.9],
            color: anticolors3[1]
        }]
    });
});

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#neduvera-vzdelani').highcharts({
        chart: {
            type: 'column',
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['']
        },
        yAxis: {
            title: {
                text: 'Nedůvěra médiím (%)'
            },
            max: 70
        },
        tooltip: {
            formatter: function() {
                return 'Médiím nevěří <b>' + this.y + ' %</b>'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'absolventi ZŠ',
            data: [48.5],
            color: colors3[3]
        }, {
            name: 'absolventi VŠ',
            data: [46.9],
            color: anticolors3[3]
        }]
    });
});

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#neduvera-vek').highcharts({
        chart: {
            type: 'column',
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['']
        },
        yAxis: {
            title: {
                text: ''
            },
            max: 70
        },
        tooltip: {
            formatter: function() {
                return 'Médiím nevěří <b>' + this.y + ' %</b>'
            }
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
            name: '18-29 let',
            data: [53.0],
            color: colors3[2]
        }, {
            name: 'nad 60 let',
            data: [44.0],
            color: anticolors3[2]
        }]
    });
});

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#neduvera-prijem').highcharts({
        chart: {
            type: 'column',
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['']
        },
        yAxis: {
            title: {
                text: ''
            },
            max: 70
        },
        tooltip: {
            formatter: function() {
                return 'Médiím nevěří <b>' + this.y + ' %</b>'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'příjem do 10 tis.',
            data: [62.3],
            color: colors3[4]
        }, {
            name: 'příjem nad 30 tis.',
            data: [46.7],
            color: anticolors3[4]
        }]
    });
});

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#neduvera-zamestnani').highcharts({
        chart: {
            type: 'column',
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['']
        },
        yAxis: {
            title: {
                text: ''
            },
            max: 70
        },
        tooltip: {
            formatter: function() {
                return 'Médiím nevěří <b>' + this.y + ' %</b>'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'nezaměstnaní',
            data: [67.3],
            color: colors3[5]
        }, {
            name: 'důchodci',
            data: [41.5],
            color: anticolors3[5]
        }]
    });
});

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#neduveravek').highcharts({
        chart: {
            type: 'column',
        },
        title: {
            text: 'Nedůvěra médiím podle věku'
        },
        subtitle: {
            text: 'Nejsilněji - o 35 procentních bodů - rostla nedůvěra k médiím ve skupině 18-29 let'
        },
        xAxis: {
            categories: ['18-29 let', '30-39 let', '40-49 let', '50-59 let', 'nad 60 let'],
            title: {
                text: ''
            }
        },
        yAxis: {
            title: {
                text: 'Nedůvěra médiím (%)'
            }
        },
        tooltip: {
            formatter: function() {
                return 'Ve skupině <b>' + this.series.name + '</b> médiím nedůvěřuje <b>' + this.y + ' %</b> lidí'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            href : '',
            text : 'Zdroj: Studie Žurnalisté ve stínu nedůvěry: k některým příčinám klesající důvěryhodnosti českých novinářů'
        },
        plotOptions: {},
        series: [{
            name: 'rok 2004',
            data: [17.7, 15.1, 19.9, 23.1, 17.5],
            color: colors[1]
        }, {
            name: 'rok 2016',
            data: [53.0, 40.5, 50.5, 42.9, 44.0],
            color: colors[5]
        }]
    });
});